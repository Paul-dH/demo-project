import { Component, NgModule } from '@angular/core';
import { ApiService } from './api.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  
  title = 'Quirky Mittens';
  dtOptions: DataTables.Settings = {};
  results;

  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
    
    this.apiService.getResults().subscribe((data)=>{
      console.log(data);
      this.results = data;
    });
    
    this.dtOptions = {
      pagingType: 'full_numbers'
    };
  }
}