import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
})
export class NavComponent implements OnInit {

  title = 'Quirky Mittens';
  isNavbarCollapsed=true;

  constructor() { }

  ngOnInit(): void { }

}
