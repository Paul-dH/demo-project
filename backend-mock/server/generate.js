const { random } = require('faker');
var faker = require('faker');

var database = { items: []};

for (var i = 1; i<= 300; i++) {
  database.items.push({
    id: i,
    material: faker.lorem.word(),
    size: faker.lorem.word(),
  });
}

console.log(JSON.stringify(database));