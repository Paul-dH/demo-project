#!/bin/sh

echo "Bringing down the docker-compose deployment"
docker-compose down -v --remove-orphans

# echo "Removing all created docker containers"
# docker ps -a | grep "local/demo-backend" | awk '{print $1}' | xargs docker rm

echo "Removing previously created docker images"
docker images -a | grep "local/demo*" | awk '{print $3}' | xargs docker rmi

echo "Build Images"
docker build -f ./frontend/Dockerfile.build -t local/demo-frontend:dev ./frontend
docker build -f ./backend/Dockerfile.build -t local/demo-backend:dev .

echo "Execute Maven tests"
docker run -it --rm -v "$PWD"/backend/build:/usr/src/app/target local/demo-backend:dev -T 1C -o test

echo "Build Maven application"
docker run -it --rm -v "$PWD"/backend/build:/usr/src/app/target local/demo-backend:dev package -T 1C -o -Dmaven.test.skip=true

echo "Build run container and Start environment"
docker-compose up -d

echo "Start logging"
docker-compose logs -f