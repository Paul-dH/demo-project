# Demo van een random Java project

Om een demo te geven van een Java App die gebuild, getest en gedeployed wordt heb ik een simpel Java project gepakt. Dit project was nog niet dockerized, maar inmiddels wel.

Elke code die naar Master wordt gepushed zal door de pipeline lopen om uiteindelijk live te staan ter test.

Het stappenplan welke ik voor de ontwikkelstraat wilde aanhouden was:
- Kies een Java app om te gebruiken als project: [originele readme](README-App.md)
- Bekijken hoe de app werkt en deze verwerken in een docker container
- Alle stappen testen om de app te testen en in de lucht te krijgen
- Gratis demo Azure VM aanvragen met Ubuntu 20.04
- Docker en Docker-Compose installeren
- Mappen structuur aanmaken
- Demo useraccount maken met SSH key
- Basis installatie (GitLab/Jenkins/Sonarqube) middels een [docker-composefile](https://gitlab.webbron.nl/DevOps/dockerhost/-/blob/master/docker-compose.yml)
- Testen of alle software werkt, incl. SSL certs en gebruikersaccount
- In GitLab een project maken met 2 repo's (1 voor de dockerhost, en 1 voor het project)
- In Jenkins de plugins installeren om GitLab repo's uit te lezen en webhooks te ontvangen.
- Pipeline project maken in Jenkins om het Javaproject te builden en lokaal te kopieren naar de Azure VM.
- Java App op de Azure VM compilen en klaarzetten in een build folder
- Java web container builden met de gecompilde app
- Via docker-compose onder de proxy de app live zetten onder een test URL

### Tot hier ben ik gekomen

- Via pipeline een code scan uitvoeren met Sonarqube en de resultaten doorsturen
- Testen of alle codescans goed verlopen binnen Sonarqube (eerder klaagde hij over niet gecompileerde classes?)
- Kijken of er codecoverage toegevoegd kan worden aan Sonarqube
- Commiten op master blokkeren en hiervoor devel / release branches maken
- Pipeline opbreken in 3 delen (ontwikkel/Test/Productie) Elk reagerend op een push naar de betreffende branch
- Koppelen SMTP server (voor status mails)
- Inrichten Grafana dashboard met [Sonarqube en Gitlab data](https://medium.com/@arunjkau/project-quality-dashboard-grafana-sonar-and-gitlab-fd784912d5e0)

### Mogelijke andere opties:
- Jenkins builds toevoegen aan het Grafana dashboard [linkje](https://medium.com/@joonasvenlinen/monitoring-jenkins-jobs-with-grafana-f1ff11af1e6c)
- [ELK stack](https://www.elastic.co/what-is/elk-stack) bekijken, wat voor toevoeging dit kan hebben
- REST call naar Philips Hue brids om de error scene te triggeren bij een failed build :P [linkje](https://github.com/peter-murray/node-hue-api)

## Project uitwerking:
Via onderstaande commando's is het project (lokaal) te builden, testen en te deployen:

```
docker build -f Dockerfile.build -t local/demo:dev .
docker run -it --rm -v "$PWD"/build:/usr/src/app/target local/demo:dev -T 1C -o test
docker run -it --rm -v "$PWD"/build:/usr/src/app/target local/demo:dev package -T 1C -o -Dmaven.test.skip=true
docker-compose up -d
```
--> Test: http://localhost:8090/swagger-ui.html
--> Test: https://test.webbron.nl/swagger-ui.html

## Tijdens het opbouwen kwam ik het volgende nog tegen:
### Jenkins user is geen lid van de docker groep in de container:
```bash
# Commando's uitvoeren in Jenkins container om gebruik te kunnen maken van de docker agent
docker exec -it -u root jenkins /bin/bash
apk --no-cache add shadow
groupadd -g 998 docker2
usermod -aG docker2 jenkins
usermod -aG docker jenkins
```
### In de jenkins container wordt de SonarQube Scanner met de hand geinstalleerd... 
*Hier moet eigenlijk netjes een apart Dockerfile voor gemaakt worden die de wijzigingen bevat, commando's aftrappen in een container kan eigenlijk echt niet... Zie: (https://medium.com/faun/setting-up-dockerized-jenkins-and-sonarqube-in-linux-environment-155ce52b884a)
```bash
mkdir -p /home/jenkins
chown -R jenkins:jenkins /home/jenkins
cd /home/jenkins
wget https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-3.3.0.1492-linux.zip
unzip sonar-scanner-cli-3.3.0.1492-linux.zip
mv sonar-scanner-3.3.0.1492-linux/ sonar-scanner
cd sonar-scanner
# Noteer het pad waar de binaries staan:
pwd
# -> /home/jenkins/sonar-scanner
```
- In de SonarQube container moest ook een commando uitgevoerd worden (voor de scanner):
```bash

```
